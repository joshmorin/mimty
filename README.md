mimty: Fast, safe, self-contained MIME Type Identification for C and Rust
==========================================================================

Mimty is a MIME type identification library for C and Rust. Both languages 
are equally supported as first class languages.

# Design Goals #
1. **Safety**: Safety guarantees are provided by the language in which this 
   library was written. Unsafe code is strictly avoided unless necessary. 
2. **Performance**: Currently, performance is achieved by avoiding heap 
   allocations. Efficient restructuring of the static MIME database is 
   planned for optimize lookup efficiency.
3. **Simplicity**: The API consists of two functions. It can't get simpler
   than this.

# MIME Database #
As much sense as completeness makes in this context, completeness is 
aimed towards by adopting the MIME type databases maintained by the Apache 
project. 


# Examples #
The following are examples of using this library. The first statement 
identifies the MIME type by the extension of the file. The second and 
third statements identify the file or blob by matching magic numbers.

In all three cases, the result is the string `"image/jpeg"`.

### C API ###

    char *mimetype = mimty_file("appendix/samples/cat.jpg");
    char *mimetype = mimty_file("appendix/samples/cat");
    char *mimetype = mimty_blob(((uint8_t [3]) {0xff, 0xd8, 0xff}), 3);
    
    
### Rust API ###

    use mimty;
    let mimetype = mimty::file("appendix/samples/cat.jpg");
    let mimetype = mimty::file("appendix/samples/cat");
    let mimetype = mimty::blob(&[0xff, 0xd8, 0xff]);


# Building #
This project makes use of Rust's build system.
 
    git clone https://joshmorin@bitbucket.org/joshmorin/mimty.git
    cd mimty
    cargo build --release

Built libraries should be in `target/release`.
The C header is located at `appendix/mimty.h`.


# Documentation #
Documentation is also handled by cargo.

    cargo doc
    
HTML documentation would be in `target/doc`


# Contribution #
For adding new MIME types, submit a patch to the Apache project; the source
of the MIME type information used by this library.

For contributing to this library, submit an issue before starting. You may also
email me at JoshMorin@gmx.com. *I can not guarantee accepting a patche if an
accompanying bug report is not submitted before hand.*

    
# Copyright #
Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
