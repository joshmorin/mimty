#!/bin/bash
# Por https://svn.apache.org/viewvc/httpd/httpd/branches/2.2.x/docs/conf/mime.types?view=markup

while read line; do
    a=($line)
    
    [[ "${line:0:1}" == '#' ]] && continue
    [[ "${#a[*]}" -lt 2 ]] && continue
    
    #/echo "${a[@]}"
    mimetype=${a[0]}
    unset a[0]
    
    for i in "${a[@]}"; do
        printf '    (%-12s %55s),\n' '"'$i'",' '"'$mimetype'\0"'
    done 
    
done
