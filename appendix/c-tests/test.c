#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "../mimty.h"

int main() {
    assert(!strcmp(mimty_file("../samples/cat"), "image/jpeg"));
    assert(!strcmp(mimty_file("../samples/cat.jpg"), "image/jpeg"));
    assert(!strcmp(mimty_blob(((uint8_t [3]) {0xff, 0xd8, 0xff}), 3), "image/jpeg"));
    puts("ok!");
    return 0;
}
