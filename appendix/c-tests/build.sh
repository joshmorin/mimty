#!/bin/bash

cargo build --release >/dev/tty 2>&1 || {
    echo "failed to build release: run 'cargo test'"
    exit 1
}
clang -g -Wall -Wextra -std=c99 -o test -L ../../target/release/ test.c -lmimty || {
    echo "Error compiling C test: Please file a bug report."
    exit 1
}
LD_LIBRARY_PATH=../../target/release ./test || {
    echo "Error running C test: Please file a bug report."
    exit 1
}
