/*
    magic.rs: Magic number MIME type associations 
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    


*/

// TODO:PER sort array for most common formats 
// TODO:FET convert apache data
// TODO:ADD proper tests

use std::io::SeekFrom;

pub struct MagicInfo {
    pub mimetype: &'static str,
    pub from: SeekFrom,
    pub magic: &'static [u8],
}

pub const MAGIC_LIST: [MagicInfo; 4] = [
    MagicInfo {
        mimetype: "image/jpeg\0",
        from: SeekFrom::Start(0),
        magic: &[0xff, 0xd8, 0xff]
    },
    MagicInfo {
        mimetype: "image/gif\0",
        from: SeekFrom::Start(0),
        magic: &[0x47, 0x49, 0x46, 0x38, 0x37, 0x61] // "GIF87a"
    },
    MagicInfo {
        mimetype: "image/gif\0",
        from: SeekFrom::Start(0),
        magic: &[0x47, 0x49, 0x46, 0x38, 0x39, 0x61] // "GIF89a"
    },
    MagicInfo {
        mimetype: "image/png\0",
        from: SeekFrom::Start(0),
        magic: &[0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a]
    },    
];
