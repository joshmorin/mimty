/*
    mimty: Fast, safe, self-contained MIME Type Identification.
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//! Fast, safe, self-contained MIME Type Identification.
//!
//! # Examples #
//! ```
//! use mimty;
//! assert!(mimty::file("appendix/samples/cat.jpg") == Some("image/jpeg"));    // by extension
//! assert!(mimty::file("appendix/samples/cat") == Some("image/jpeg"));        // by magic number
//! assert!(mimty::blob(&[0xff, 0xd8, 0xff]) == Some("image/jpeg"));           // by magic number
//! ```
//! 
//! # Copyright #
//! Copyright © 2015  Josh Morin <JoshMorin@gmx.com>
//! 
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the GNU Lesser General Public License as published by
//! the Free Software Foundation, either version 3 of the License, or
//! (at your option) any later version.
//! 
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU Lesser General Public License for more details.
//! 
//! You should have received a copy of the GNU General Public License
//! along with this program.  If not, see <http://www.gnu.org/licenses/>.

// DEV README
// ----------
// Trailing nul bytes have been added to the MIME type tables to avoid 
// unnecessary allocations or copying in the C API
// 

use std::path::{Path};
use std::fs::{File};
use std::io::{Seek, Read};

mod ext;
mod magic;
pub mod ffi;
use ext::*;
use magic::*;

/// Result of the file identification attempt
pub type MimeType = Option<&'static str>;


/// Attempt to identify a binary blob by its content
/// 
/// This function will return None if it can not identify the MIME
/// type of the blob.
pub fn blob(blob: &[u8]) -> MimeType {
    use std::io::Cursor;
    let mut blob = Cursor::new(blob);
    for i in MAGIC_LIST.iter() {
        let mut buf: [u8; 60] = [0; 60];
        if blob.seek(i.from).is_err() {continue}
        if blob.read(&mut buf[..i.magic.len()]).is_err() {continue}
        if &buf[..i.magic.len()] == i.magic {
            // XXX Fix when `slice_chars` stabilizes
            // removing the trailing nul from mimetype
            // check is not necessary unless extension is not associated with 
            // a MIME type
            let l = i.mimetype.len();
            if l > 0 {
                return Some(unsafe{i.mimetype.slice_unchecked(0, l-1)});
            }
            else {
                return None
            }
        }
    }
    return None;
}

/// Attempt to identify a file by its extension and contents
///
/// When identifying the MIME type of a file, proceedence is given to 
/// the extension of the file. If an extension is present, the library 
/// will not attempt to investigate further; i.e. the file does not need
/// to exist for identification to succeed.
///
/// If an extension is not present, the library will access the file to
/// identify it by its contents.
/// 
/// This function will return None if the file does not exist, the 
/// process does not have sufficiant permissions to read the file, 
/// or if the MIME type of the file could not be determined.

pub fn file<P: AsRef<Path>>(path: P) -> MimeType {
    let path: &Path = path.as_ref();
    
    // If the path has an extension
    if let Some(ext) = path.extension() {
        // that can be converted to a string
        if let Some(ext) = ext.to_str() {
            // and the extension is in EXTENSION_LIST
            if let Ok(i) = EXTENSION_LIST.binary_search_by(|r| r.0.cmp(ext)) {
                let mime = &EXTENSION_LIST[i].1;
                // return that mime type 
                // XXX Fix when `slice_chars` stabilizes
                // removing the trailing nul from mimetype
                // check is not necessary unless extension is not associated with 
                // a MIME type
                // XXX Handle multiple mimes
                let l = mime[0].len();
                if l > 0 {
                    return Some(unsafe{mime[0].slice_unchecked(0, l-1)});
                }
                else {
                    return None
                }
            }
        }
    }
    
    // otherwise try and determine the file type from its magic number
    if let Ok(mut fd) = File::open(&path) {
        for i in MAGIC_LIST.iter() {
            let mut buf: [u8; 60] = [0; 60];
            if fd.seek(i.from).is_err() {continue}
            if fd.read(&mut buf[..i.magic.len()]).is_err() {continue}
            if &buf[..i.magic.len()] == i.magic {
                // XXX Fix when `slice_chars` stabilizes
                // removing the trailing nul from mimetype
                // check is not necessary unless extension is not associated with 
                // a MIME type
                let l = i.mimetype.len();
                if l > 0 {
                    return Some(unsafe{i.mimetype.slice_unchecked(0, l-1)});
                }
                else {
                    return None
                }
            }
        }
    }
    
    // Neither method works
    return None;
}
// These tests must be carried out from the project's root. Cargo knows what
// to do.
#[test]
fn peliminary_test(){
    assert!(file("appendix/samples/monkey") == Some("image/jpeg"));
    assert!(file("appendix/samples/monkey.jpg") == Some("image/jpeg"));
    assert!(file("/some/none/existent/path.jpg") == Some("image/jpeg"));
    assert!(file("/some/none/existent/path") == None);
}
