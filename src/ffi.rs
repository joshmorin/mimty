//! C function interface for this library

use ::{file, blob};
use std::ffi::CStr;
use std::ptr::null;
use std::slice;
use std::str;

/// Attempts to determine file MIME type by either examining the files 
/// extension, then by examining its content if extension identification 
/// was unsuccessful.
/// 
/// ```C
/// char* mimty_file(char *path);
/// ```
///
/// Upon success, the MIME type of the file will be returned. Null is 
/// returned otherwise. This function takes and returns null-terminated 
/// strings compatible with C. Returned strings remain valid for the life of
/// the library.
///
/// #Note
/// At present, paths that contain invalid utf8 will cause this function to
/// return null. This will be fixed as soon as from_osstr or as_osstr 
/// functionality is implemented for [u8]. If this is an issue, consider 
/// passing a dummy filename with a matching extension and using `mimty_blob`
/// to identify the contents.
#[no_mangle]
pub extern fn mimty_file(path: *const u8) -> *const u8 {
    let path = unsafe {CStr::from_ptr(path as *const i8)};
    let path = match str::from_utf8(path.to_bytes()) {
        Ok(p) => p, 
        _ => return null()
    };
    match file(path) {
        Some(a) => a.as_bytes().as_ptr(),
        None => null()
    }
}

/// Attempts to determine blob MIME type by examining its content.
///
/// ```C
/// char* mimty_blob(uint8_t *blob, uint32_t length);
/// ```
///
/// Upon success, the MIME type of the file will be returned. Null is 
/// returned otherwise. This function returns null-terminated strings 
/// compatible with C. Returned strings remain valid for the life of
/// the library.
#[no_mangle]
pub extern fn mimty_blob(b: *const u8, length: u32) -> *const u8 {
    let b = unsafe {slice::from_raw_parts(b, length as usize)};
    match blob(b) {
        Some(a) => a.as_bytes().as_ptr(),
        None => null()
    }
}
